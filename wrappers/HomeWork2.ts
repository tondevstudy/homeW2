import {
    Address,
    beginCell,
    Cell,
    Contract,
    contractAddress,
    ContractProvider,
    Sender,
    SendMode,
    toNano
} from '@ton/core';

export type HomeWork2Config = {
    id: number; //for address generation only
    admin: Address;
};

export function homeWork2ConfigToCell(config: HomeWork2Config): Cell {
    return beginCell().
        storeAddress(config.admin).
        storeUint(toNano('2'), 64).
        storeInt(config.id, 32).
    endCell();
}

export const Opcodes = {
    // const op::accept = "op::accept"c; ;;29390d60
    // const op::withdraw = "op::withdraw"c; ;;cb03bfaf
    // const op::set_admin = "op::set_admin"c; ;;4d5f2cca
    // const op::set_min_accept_amount = "op::set_min_accept_amount"c; ;;403add28
    accept: 0x29390d60,
    withdraw: 0xcb03bfaf,
    set_admin: 0x4d5f2cca,
    set_min_accept_amount: 0x403add28,
};

export class HomeWork2 implements Contract {
    constructor(readonly address: Address, readonly init?: { code: Cell; data: Cell }) {}

    static createFromAddress(address: Address) {
        return new HomeWork2(address);
    }

    static createFromConfig(config: HomeWork2Config, code: Cell, workchain = 0) {
        const data = homeWork2ConfigToCell(config);
        const init = { code, data };
        return new HomeWork2(contractAddress(workchain, init), init);
    }

    async sendDeploy(provider: ContractProvider, via: Sender, value: bigint) {
        await provider.internal(via, {
            value,
            sendMode: SendMode.PAY_GAS_SEPARATELY,
            body: beginCell().endCell(),
        });
    }

    async sendAccept(
        provider: ContractProvider,
        via: Sender,
        opts: {
            value: bigint;
        }
    ) {
        await provider.internal(via, {
            value: opts.value,
            sendMode: SendMode.PAY_GAS_SEPARATELY,
            body: beginCell()
                .storeUint(Opcodes.accept, 32)
                .endCell(),
        });
    }

    async sendUpdateMinAmount(
        provider: ContractProvider,
        via: Sender,
        opts: {
            value: bigint;
            newMin: bigint;
        }
    ) {
        await provider.internal(via, {
            value: opts.value,
            sendMode: SendMode.PAY_GAS_SEPARATELY,
            body: beginCell()
                .storeUint(Opcodes.set_min_accept_amount, 32)
                .storeInt(opts.newMin, 64) // .storeCoins(opts.newMin)?
                .endCell(),
        });
    }

    async sendUpdateAdmin(
        provider: ContractProvider,
        via: Sender,
        opts: {
            value: bigint;
            newAdmin: Address;
        }
    ) {
        await provider.internal(via, {
            value: opts.value,
            sendMode: SendMode.PAY_GAS_SEPARATELY,
            body: beginCell()
                .storeUint(Opcodes.set_admin, 32)
                .storeAddress(opts.newAdmin) // .storeCoins(opts.newMin)?
                .endCell(),
        });
    }

    async sendWithdraw(
        provider: ContractProvider,
        via: Sender,
        opts: {
            value: bigint;
            amount: bigint;
        }
    ) {
        await provider.internal(via, {
            value: opts.value,
            sendMode: SendMode.PAY_GAS_SEPARATELY,
            body: beginCell()
                .storeUint(Opcodes.withdraw, 32)
                .storeCoins(opts.amount)
                .endCell(),
        });
    }

    async getMinAcceptAmount(provider: ContractProvider) {
        const result = await provider.get('get_min_accept_amount', []);
        return result.stack.readBigNumber();
    }

    async getAdminAddress(provider: ContractProvider) {
        const result = await provider.get('get_admin_address', []);
        return result.stack.readAddress();
    }

    async getBalance(provider: ContractProvider) {
        const {stack} = await provider.get("balance",[]);
        return stack.readBigNumber(); //{balance: stack.readBigNumber()}
    }

    async getId(provider: ContractProvider) {
        const {stack} = await provider.get("get_contract_id",[]);
        return {contract_id: stack.readNumber()};
    }
}
