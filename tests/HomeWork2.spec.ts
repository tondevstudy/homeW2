import { Blockchain, printTransactionFees, SandboxContract, TreasuryContract } from '@ton/sandbox';
import { Address, Cell, toNano } from '@ton/core';
import { HomeWork2 } from '../wrappers/HomeWork2';
import '@ton/test-utils';
import { compile } from '@ton/blueprint';

interface Transaction {
    totalFees: {coins :bigint};
    // other fields can be added here
}

describe('HomeWork2', () => {
    let code: Cell;

    beforeAll(async () => {
        code = await compile('HomeWork2');
    });

    let blockchain: Blockchain;
    let deployer: SandboxContract<TreasuryContract>;
    let homeWork2: SandboxContract<HomeWork2>;


    let user1: SandboxContract<TreasuryContract>;
    let user2: SandboxContract<TreasuryContract>;
    let user3: SandboxContract<TreasuryContract>;
    const sumTotalFees = (transactions: Transaction[]): bigint => {
        return transactions.reduce((sum, transaction) => sum + transaction.totalFees.coins, 0n);
    };
    const initTon = toNano('0.1');
    beforeEach(async () => {
        blockchain = await Blockchain.create();
        deployer = await blockchain.treasury('deployer');

        homeWork2 = blockchain.openContract(
            HomeWork2.createFromConfig(
                {
                    id: Math.floor(Math.random() * 10000),
                    admin: deployer.address,
                },
                code
            )
        );

        user1 = await blockchain.treasury('user1', {balance: toNano(2)});
        user2 = await blockchain.treasury('user2');
        user3 = await blockchain.treasury('user3');
        const deployResult = await homeWork2.sendDeploy(deployer.getSender(), initTon);

        expect(deployResult.transactions).toHaveTransaction({
            from: deployer.address,
            to: homeWork2.address,
            deploy: true,
            success: true,
        });
    });

    it('should return init admin addr', async () => {
        const admin = await homeWork2.getAdminAddress();
        expect(deployer.address.toString()).toBe(admin.toString());
    });


    it('should accept ton and store on balance', async () => {
        let incTon = toNano('5');
        const acceptResult = await homeWork2.sendAccept(deployer.getSender(), {value:incTon});
        let gasFees = sumTotalFees(acceptResult.transactions);
        let calculatedBalance =  (await homeWork2.getBalance() + gasFees) - (initTon + incTon);
        expect(toNano('0.01')).toBeGreaterThan(calculatedBalance);
        expect(calculatedBalance).toBeGreaterThan(0);

        let incTon2 = toNano('3');
        const acceptResult2 = await homeWork2.sendAccept(deployer.getSender(), {value:incTon2});
        gasFees = sumTotalFees(acceptResult2.transactions);
        calculatedBalance =  (await homeWork2.getBalance() + gasFees) - (initTon + incTon + incTon2);
        expect(toNano('0.01')).toBeGreaterThan(calculatedBalance);
        expect(calculatedBalance).toBeGreaterThan(0);

    });

    it('should reject less than 2 ton by default', async () => {
        let incTon = toNano('1');
        const acceptResult = await homeWork2.sendAccept(deployer.getSender(), {value:incTon});
        let gasFees = sumTotalFees(acceptResult.transactions);
        let val = await homeWork2.getBalance();
        expect(toNano('1')).toBeGreaterThan(val);
        // expect(val).toBeGreaterThan(0);


        let incTon2 = toNano('1.98');
        const acceptResult2 = await homeWork2.sendAccept(deployer.getSender(), {value:incTon2});
        gasFees = sumTotalFees(acceptResult2.transactions);
        val = await homeWork2.getBalance();
        expect(toNano('1.5')).toBeGreaterThan(val);
        expect(val).toBeGreaterThan(0);
    });


    it('should be able to change min accept amount and accept according to new min', async () => {
        let incTon = toNano('1');
        const acceptResult = await homeWork2.sendAccept(deployer.getSender(), {value:incTon});
        let gasFees = sumTotalFees(acceptResult.transactions);
        let rejectFee = toNano('0.1');
        let val = initTon + rejectFee - await homeWork2.getBalance();
        expect(toNano('0.01')).toBeGreaterThan(val);
        expect(val).toBeGreaterThan(0);

        let beforeAmount = await homeWork2.getMinAcceptAmount();
        const changeMin = await homeWork2.sendUpdateMinAmount(deployer.getSender(), {value:toNano('0.1'), newMin: incTon});
        let afterAmount = await homeWork2.getMinAcceptAmount();

        expect(afterAmount).toBe(incTon);

        let bef = await homeWork2.getBalance();
        const acceptResultAfter = await homeWork2.sendAccept(deployer.getSender(), {value:incTon});
        let aft = await homeWork2.getBalance();

        // gasFees = sumTotalFees(acceptResult.transactions);
        let val2 = bef + incTon - aft;
        expect(toNano('0.01')).toBeGreaterThan(val2);
        expect(val2).toBeGreaterThan(0);
    });


    it('should be able to withdraw to admin and change to new admin', async () => {
        let incTon = toNano('10');
        const acceptResult = await homeWork2.sendAccept(deployer.getSender(), {value:incTon});
        expect(await homeWork2.getBalance()).toBeGreaterThan(incTon);

        const withdrawRes = await homeWork2.sendWithdraw(deployer.getSender(), {value:incTon, amount: toNano('5')});

        expect(withdrawRes.transactions).toHaveTransaction({
            from: homeWork2.address,
            to: deployer.address,
            success: true,
            value: toNano("5")
        });
    });

    it('should not be able to withdraw to 3rd party acc', async () => {
        let incTon = toNano('10');
        const acceptResult = await homeWork2.sendAccept(deployer.getSender(), {value:incTon});
        expect(await homeWork2.getBalance()).toBeGreaterThan(incTon);

        const withdrawRes = await homeWork2.sendWithdraw(user1.getSender(), {value:incTon, amount: toNano('5')});

        expect(withdrawRes.transactions).not.toHaveTransaction({
            from: homeWork2.address,
            to: user1.address,
            success: true,
            value: toNano("5")
        });
    });

    it('should not be able to change admin to 3rd party acc', async () => {
        let incTon = toNano('10');
        const acceptResult = await homeWork2.sendAccept(deployer.getSender(), {value:incTon});
        expect(await homeWork2.getBalance()).toBeGreaterThan(incTon);

        const withdrawRes = await homeWork2.sendWithdraw(user1.getSender(), {value:incTon, amount: toNano('5')});

        expect(withdrawRes.transactions).not.toHaveTransaction({
            from: homeWork2.address,
            to: user1.address,
            success: true,
            value: toNano("5")
        });

        const changeAdmin = await homeWork2.sendUpdateAdmin(deployer.getSender(), {value:toNano('0.1'), newAdmin: user1.address});

        let balbefore = await user1.getBalance();
        console.log(balbefore);
        const withdrawResTry2 = await homeWork2.sendWithdraw(user1.getSender(), {value:toNano('0.1'), amount: toNano('5')});
        let balafter = await user1.getBalance();
        console.log(balafter);
        expect(withdrawResTry2.transactions).toHaveTransaction({
            from: homeWork2.address,
            to: user1.address,
            success: true,
            value: toNano("5")
        });
    });


});
