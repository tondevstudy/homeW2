// import { Address, toNano } from '@ton/core';
// import { HomeWork2 } from '../wrappers/HomeWork2';
// import { NetworkProvider, sleep } from '@ton/blueprint';
//
// export async function run(provider: NetworkProvider, args: string[]) {
//     const ui = provider.ui();
//
//     const address = Address.parse(args.length > 0 ? args[0] : await ui.input('HomeWork2 address'));
//
//     if (!(await provider.isContractDeployed(address))) {
//         ui.write(`Error: Contract at address ${address} is not deployed!`);
//         return;
//     }
//
//     const homeWork2 = provider.open(HomeWork2.createFromAddress(address));
//
//     const counterBefore = await homeWork2.getCounter();
//
//     await homeWork2.sendIncrease(provider.sender(), {
//         increaseBy: 1,
//         value: toNano('0.05'),
//     });
//
//     ui.write('Waiting for counter to increase...');
//
//     let counterAfter = await homeWork2.getCounter();
//     let attempt = 1;
//     while (counterAfter === counterBefore) {
//         ui.setActionPrompt(`Attempt ${attempt}`);
//         await sleep(2000);
//         counterAfter = await homeWork2.getCounter();
//         attempt++;
//     }
//
//     ui.clearActionPrompt();
//     ui.write('Counter increased successfully!');
// }
